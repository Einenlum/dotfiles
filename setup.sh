#!/bin/sh

if [[ -z $2 ]]
then
    echo -e "This setup script must receive two arguments.\nThe first recieves \"normal\" or \"light\", the second receives \"fix\" or \"laptop\"."
    echo -e "Example of valid use: sudo ./dotfiles/setup.sh light laptop"
    exit 1
fi

if [[ "$1" != "light" ]] && [[ "$1" != "normal" ]]
then
    echo "The first argument must be \"normal\" or \"light\""
    exit 1
fi

if [[ "$2" != "fix" ]] && [[ "$2" != "laptop" ]]
then
    echo "The second argument must be \"fix\" or \"laptop\""
    exit 1
fi

echo "Installing dependencies, please wait ..."

user=`whoami`

echo "zsh"
sudo apt-get install -y zsh
echo "😄 done"
echo "openssh"
sudo apt-get install -y openssh-client openssh-server
echo "😄 done"
echo "mysql ..."
sudo apt-get install -y mysql-server
echo "😄 done"
echo "git guake i3 vim curl ruby php5 nitrogen pcmanfm..."
sudo apt-get install -y git guake i3 vim vim-gnome curl ruby2.2-dev php5 php5-intl php5-json php5-mcrypt php5-xdebug php5-mysql nitrogen pcmanfm
echo "😄 done"
echo "sublime-text ..."
sudo add-apt-repository -y ppa:webupd8team/sublime-text-3
sudo apt-get update
sudo apt-get install -y sublime-text-installer
echo "😄 done"
echo "nodejs ..."
sudo add-apt-repository -y ppa:chris-lea/node.js
sudo apt-get update
sudo apt-get install -y nodejs npm
sudo chown -R $(whoami) "~/.npm"
sudo ln -s /usr/bin/nodejs /usr/bin/node
echo "😄 done"
echo "composer ..."
curl -sS https://getcomposer.org/installer | php
sudo mv composer.phar /usr/bin/composer && sudo chmod +x /usr/bin/composer
echo "😄 done"
echo "phantomjs ..."
sudo npm install -g phantomjs
echo "😄 done"
echo "grunt ..."
sudo npm install -g grunt-cli
echo "😄 done"
echo "gulp ..."
sudo npm install -g gulp
echo "😄 done"
echo "bower ..."
sudo npm install -g bower
echo "😄 done"
echo "guard ..."
sudo apt-get install -y exuberant-ctags inotify-tools
gem install --user guard-shell
sudo sysctl fs.inotify.max_user_watches=32768
echo "😄 done"
echo "tmuxinator ..."
sudo apt-get install -y tmux
gem install --user tmuxinator
echo "😄 done"
echo "oh-my-zsh ..."
wget --no-check-certificate http://install.ohmyz.sh -O - | sh
echo "😄 done"
echo "xflux ..."
sudo add-apt-repository -y ppa:kilian/f.lux
sudo apt-get update
sudo apt-get install -y fluxgui
echo "😄 done"

########## Variables

olddir=~/dotfiles_old             # old dotfiles backup directory
files=`ls -1a ~/dotfiles/symlinks`
##########

## sublime
cd ~/.config/sublime-text-3/Packages/ && git clone https://github.com/electricgraffitti/soda-solarized-dark-theme "Theme - Soda SolarizedDark SolarizedDark"
cd ~
ln -s ~/dotfiles/symlinks/miscellaneous/sublime/Preferences.sublime-settings ~/.config/sublime-text-3/Packages/User/Preferences.sublime-settings

# create dotfiles_old in homedir
echo "Creating $olddir for backup of any existing dotfiles in ~"
mkdir -p $olddir
echo "...done"

# move any existing dotfiles in homedir to dotfiles_old directory, then create symlinks 
for file in $files; do
    echo "Moving any existing dotfiles from ~ to $olddir"
    mv ~/$file ~/dotfiles_old/
    echo "Creating symlink to $file in home directory."
    if [[ "$file" != "." ]] && [[ "$file" != ".." ]]
    then
        ln -s ~/dotfiles/symlinks/$file ~/$file
    fi
done

if [[ "$1" = "light" ]]
then
    files=`ls -1a ~/dotfiles/symlinks-light`
    for file in $files; do
        if [[ "$file" != "." ]] && [[ "$file" != ".." ]]
        then
        echo "Creating symlink to $file in home directory."
        ln -s ~/dotfiles/symlinks-light/$file ~/$file
        fi
    done
fi

if [[ "$2" = "laptop" ]]
then
    sudo apt-get install -y xbacklight
    echo "laptop"
    files=`ls -1a ~/dotfiles/symlinks-laptop`
    for file in $files; do
        if [[ "$file" != "." ]] && [[ "$file" != ".." ]]
        then
            echo "Creating symlink to $file in home directory."
            ln -s ~/dotfiles/symlinks-laptop/$file ~/$file
        fi
    done
fi

## vim
git clone https://github.com/gmarik/Vundle.vim.git ~/.vim/bundle/Vundle.vim
mkdir ~/.vim/backup
