let s:indentation = 0
let s:indentationFound = 0

" We stop when we have the first indentation
function! CheckIndentation()
    if s:indentationFound == 0
        let s:indentation = indent(".")
    endif

    if s:indentation != 0
        let s:indentationFound = 1
    endif
endfunction

" We launch the script on all non empty lines
function! CheckIndentations(lineNumber)
    let lineMax = a:lineNumber
    execute 0, lineMax/^ /:call CheckIndentation()"
endfunction

function! GetKeySequence(lineNumber)
    :call CheckIndentations(a:lineNumber)
    let currentKey = matchstr(getline(a:lineNumber), '\s\+\zs.\+\ze:')
    let s:keys = [currentKey, 'test']

    echo join(s:keys, " > ")
endfunction

nmap ,ind :call GetKeySequence(line("."))<CR>
