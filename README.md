Dotfiles
=========

To install this configuration, clone this repository in your home directory.

    git clone git@gitlab.com:Einenlum/dotfiles.git
    
Then, launch the setup bash script with root privileges.

    sudo bash dotfiles/setup.py (normal|light) (fix|laptop)

It takes two mandatory arguments:

- For the first one:
    - normal
    - light (for not powerful computers)
- For the second one:
    - fix (for stationary pc)
    - laptop (for laptop pc)